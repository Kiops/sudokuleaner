import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
Item {
    id: mainItem
    property color border_collor: "#9E9E9E"
    width: arrow.width + mainRect.width
    height: mainRect.height
    state: "right-oriented"
    property real sizeOfMatrixCell
    property alias model: digitsModel
    property var digits_order: [7,8,9,4,5,6,1,2,3]
    property bool textMode: false
    function setAllowedDigits(allowedDigits)
    {
        var elem = digitsModel.get(0)
        for(var i = 0; i < 9; i++)
        {
            digitsModel.get(i)["faded"] = true;
        }
        for(var j = 0; j < allowedDigits.length; j++)
        {
            digitsModel.get(digits_order.indexOf(allowedDigits[j]))["faded"] = false;
        }
    }
    OpacityAnimator{
        target: mainItem
        from: 0
        to: 1
        duration: 5000
    }
    
    Arrow{
        id: arrow
        width: arrowWidth/2
        height: arrowWidth/2
        arrowWidth: mainRect.width/3
        arrowHeight: arrowWidth/2
        rotation: -90
        color: mainItem.border_collor
    }

    Rectangle{
        color: "white"
        id: mainRect
        width: 100 * mainItem.sizeOfMatrixCell / 50
        height: width
        property real cellSize: width/3
        border.color: mainItem.border_collor
        Text{
            visible: textMode
            id: textElement
            anchors.fill: parent
            text: "Удалить догадку (backspace)"
            width: parent.width
            horizontalAlignment: Text.AlignHCenter 
            verticalAlignment: Text.AlignVCenter
            fontSizeMode: Text.Fit
            wrapMode: Text.WordWrap
        }

        GridView{
            visible: !mainItem.textMode
            cellHeight: mainRect.cellSize
            cellWidth: mainRect.cellSize
            anchors.fill: parent
            interactive: false
            model: ListModel{
                id: digitsModel
                Component.onCompleted: {
                    
                    for(var i = 0; i < mainItem.digits_order.length; i++)
                        append({"number": mainItem.digits_order[i], "faded": false, "correct": false})
                }
            }
            
            delegate:
                Item {
                width: mainRect.cellSize
                height: mainRect.cellSize
                Text {
                    font.pointSize: 26 * mainRect.cellSize/50
                    anchors.centerIn: parent
                    text: number
                    color: faded ? "#9E9E9E" : "black"
                }
            }
        }
    }
    
    states: [
        State {
            name: "left-oriented"
            PropertyChanges {
                target: arrow
                rotation: -90
            }
            PropertyChanges{
                target: mainItem
                width: arrow.width + mainRect.width
                height: mainRect.height
            }
            AnchorChanges {
                target: arrow
                anchors.verticalCenter: mainItem.verticalCenter
                anchors.left: mainItem.left
                anchors.right: undefined
                anchors.bottom: undefined
                anchors.top: undefined
                anchors.horizontalCenter: undefined
            }
            AnchorChanges {
                target: mainRect
                anchors.left: arrow.right
                anchors.verticalCenter: mainItem.verticalCenter
                anchors.right: undefined
                anchors.bottom: undefined
                anchors.top: undefined
                anchors.horizontalCenter: undefined
            }
        },
        State {
            name: "right-oriented"
            PropertyChanges {
                target: arrow
                rotation: 90
            }
            PropertyChanges{
                target: mainItem
                width: arrow.width + mainRect.width
                height: mainRect.height
            }
            AnchorChanges {
                target: arrow
                anchors.right: mainItem.right
                anchors.verticalCenter: mainItem.verticalCenter
                anchors.left: undefined
                anchors.bottom: undefined
                anchors.top: undefined
                anchors.horizontalCenter: undefined
            }
            AnchorChanges {
                target: mainRect
                anchors.right: arrow.left
                anchors.verticalCenter: mainItem.verticalCenter
                anchors.left: undefined
                anchors.bottom: undefined
                anchors.top: undefined
                anchors.horizontalCenter: undefined
            }
        },
        State {
            name: "top-oriented"
            PropertyChanges {
                target: arrow
                rotation: 0
            }
            PropertyChanges{
                target: mainItem
                width: mainRect.width
                height: arrow.height + mainRect.height
            }
            AnchorChanges {
                target: arrow
                anchors.right: undefined
                anchors.verticalCenter: undefined
                anchors.left: undefined
                anchors.bottom: undefined
                anchors.top: mainItem.top
                anchors.horizontalCenter: mainItem.horizontalCenter
            }
            AnchorChanges {
                target: mainRect
                anchors.right: undefined
                anchors.verticalCenter: undefined
                anchors.left: undefined
                anchors.bottom: undefined
                anchors.top: arrow.bottom
                anchors.horizontalCenter: mainItem.horizontalCenter
            }
        },
        State {
            name: "bottom-oriented"
            PropertyChanges {
                target: arrow
                rotation: 180
            }
            PropertyChanges{
                target: mainItem
                width: mainRect.width
                height: arrow.height + mainRect.height
            }
            AnchorChanges {
                target: arrow
                anchors.right: undefined
                anchors.verticalCenter: undefined
                anchors.left: undefined
                anchors.bottom: mainItem.bottom
                anchors.top: undefined
                anchors.horizontalCenter: mainItem.horizontalCenter
            }
            AnchorChanges {
                target: mainRect
                anchors.right: undefined
                anchors.verticalCenter: undefined
                anchors.left: undefined
                anchors.bottom: arrow.top
                anchors.top: undefined
                anchors.horizontalCenter: mainItem.horizontalCenter
            }
        }
    ]
}
