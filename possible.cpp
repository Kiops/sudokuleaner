#include "possible.h"


Possible::Possible()
{
    clear();
}

//translates x y to index of square
int Possible::getSquareIndex(int x, int y){
    return (x/3)+3*(y/3);
}

bool Possible::veirfy(int x, int y, int value)
{
    value -=1;
    if(horisontal[y][value] == false || 
    vertical[x][value] == false || 
    square[getSquareIndex(x, y)][value] == false)
        return false;
    return true;
}

//returns list of possible digits in x, y
QVector<int> Possible::getPossible(int x, int y) const{
    QVector<int> result;
    auto sIndex = getSquareIndex(x,y);
    for(int i = 0; i < 9; i++)
    {
        if( horisontal[y][i] && vertical[x][i] && square[sIndex][i])
            result.push_back(i+1);
    }
    return result;
}

//Sets Possible to initial state
void Possible::clear(){
    horisontal = vertical = square = QVector<QVector<bool>>(9, QVector<bool>(9, true));
}

//Tells to possible that value was set into x, y
//this must be done after every correcting of matrix
void Possible::notify(int x, int y, int value, bool avaliable)
{
    if(!veirfy(x, y, value) && !avaliable)
        throw PossibleException("Wrong input");
    value -=1;
    horisontal[y][value] = avaliable;
    vertical[x][value] = avaliable;
    square[getSquareIndex(x, y)][value] = avaliable;
}


//sets possible values according to matrix
//calling it will fully reset Possible and set it to appropriate values
void Possible::scanMatrix(QVector<QVector<int>> & matrix)
{
    horisontal = vertical = QVector<QVector<bool>>(9, QVector<bool>(9, true));
    square = vertical = QVector<QVector<bool>>(9, QVector<bool>(9, true));
    vertical = QVector<QVector<bool>>(9, QVector<bool>(9, true));
    
    for(int i = 0; i < matrix.size(); i++)
    {
        for(int j = 0; j < matrix[i].size(); j++)
        {
            if(matrix[i][j] > 0)
                notify(j, i, matrix[i][j]);
        }
    }
}

QList<int> Possible::getPossibleForm(const int& x, const int& y, const Form& form)
{
    //get row, column or rectangle of possible elements
    QVector<bool>* desired_container;
    if(form == VERTICAL)
        desired_container = &vertical[x];
    else if(form == HORISONTAL)
        desired_container = &horisontal[y];
    else 
        desired_container = &square[getSquareIndex(x, y)]; //SQUARE case
    
    //form result
    QList<int> result;
    for(int i = 0; i < desired_container->size(); i++)
        if(desired_container->operator[](i) == true)
            result.push_back(i+1);
    
    return result;    
}

QList<int> Possible::getPossibleInColumn(const int& x)
{
    return getPossibleForm(x, 0, VERTICAL);
}

QList<int> Possible::getPossibleInRow(const int& y)
{
    return getPossibleForm(0, y, HORISONTAL);
}

QList<int> Possible::getPossibleInRectangle(const int& x, const int& y)
{
    return getPossibleForm(x, y, SQUARE);
}
