import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
import Sudoku 1.0
import QtGraphicalEffects 1.0

Item {
    id: mainItem
    height: matrix.height + 2 * column_helper.height
    width: matrix.width + row_helper.width + rect_helper.width
    property bool custom_initial: false
    function prepareSudoku(){ sudoku.prepare()}
    function clearAll(){
        matrix.clear();
        sudoku.clear(true);
    }
    
    Popup {
        id: popup_error
        width: popup_error_text.width*1.5
        height: popup_error_text.height*1.5
        anchors.centerIn: parent
        modal: true
        focus: true
        Text{
            id: popup_error_text
            text: "Ошибка в условии"
            font.pointSize: 23
            anchors.centerIn: parent
        }
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
    }

    enum Figures{
        ROW, COLUMN, SQUARE
    }
    
    function pointHelpers(x, y)
    {
        row_helper.y = matrix.y - row_helper.height/2 +  matrix.cellHeight * y + matrix.cellHeight/2;
        column_helper.x = matrix.x - column_helper.width/2 + matrix.cellWidth * x + matrix.cellWidth/2;
        rect_helper.y = matrix.y + 0.5*matrix.cellHeight + 3*matrix.cellHeight * ((y - y % 3)/3)
        anim.set_and_start(x,y)
    }
    
    function pointFallbackHelper(x, y)
    {
        console.log("Pointing")
        fallback_helper.x = matrix.x - fallback_helper.width/2 + matrix.cellWidth * x + matrix.cellWidth/2
        fallback_anim.set_and_start(y)
    }
    
    Popup {
        id: popup
        width: popup_column.width*1.2
        height: popup_column.height*1.2
        anchors.centerIn: parent
        modal: true
        focus: true
        property var start_time
        property var end_time
        property string user_time: msToSTring(end_time - start_time)
        property int mistakes_count: 0
        function msToSTring(ms)
        {
            var result = ''
            var seconds = Math.floor(ms/1000)
            var minutes = Math.floor(seconds/60)
            var hours = Math.floor(minutes/60)
            ms = ms - seconds*1000
            seconds = seconds - minutes * 60
            minutes = minutes - hours * 60
            result += hours > 0 ? hours + " ч " : ''
            result += minutes > 0 ? minutes + " мин " : ''
            result += seconds > 0 ? seconds + " с " : ''
            result += ms > 0 ? ms + " мс" : ''
            return result
        }
        function getSkill()
        {
            var total = sudoku.getSizeOfLoggedSteps()
            var percentage = mistakes_count/total
            if(percentage === 0)
                return "превосходный"
            else if(percentage < 0.02)
                return "высокий"
            else if(percentage < 0.05)
                return "средний"
            else if(percentage < 0.25)
                return "низкий"
            else
                return "ничтожный"
        }
        
        Column{
            id: popup_column
            anchors.centerIn: parent
            Text{
                id: popup_text
                text: "Судоку решено"
                font.pointSize: 15
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Text{
                id: popup_text1
                text: "Время решения: " + popup.user_time
                font.pointSize: 15
            }
            Text{
                id: popup_text2
                text: "Количество ошибок: " + popup.mistakes_count
                font.pointSize: 15
            }
            Text{
                id: popup_text3
                text: "Навык решения: " + popup.getSkill()
                font.pointSize: 15
            }
        }
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
    }
    
    NumberAnimation {
        id: hider_anim
        targets: [row_helper, column_helper, rect_helper]
        properties: "opacity"
        to: 0.0
        duration: 400
        onStarted: {    
            anim.stop();
            mainItem.paint_over(-1,-1);
            
        }
        easing.type: Easing.Linear
    }
    
    NumberAnimation {
        id: hider_fallback_anim
        target: fallback_helper
        properties: "opacity"
        to: 0.0
        duration: 400
        onStarted: {    
            fallback_anim.stop();
            fallback_anim.isSeen = false
        }
        easing.type: Easing.Linear
    }
    
    ParallelAnimation{
        id: fallback_anim
        property real y
        property bool isSeen: false
        function set_and_start(y)
        {
            if(!isSeen)
            {
                fallback_anim.y = y;
                isSeen = true;
                fallback_anim.start();
            }
        }
        
        NumberAnimation {
            target: fallback_helper
            property: "opacity"
            from: 0.0
            to: 1.0
            duration: 500
            easing.type: Easing.InOutQuad
        }
        NumberAnimation {
            target: fallback_helper
            property: "y"
            //            from: matrix.y - fallback_helper.height - anim.distance
            from: matrix.y + matrix.cellHeight * (fallback_anim.y + 1) + anim.distance
            to: matrix.y + matrix.cellHeight * (fallback_anim.y + 1)
            duration: 500
            easing.type: Easing.InOutQuad
        }
    }
    
    SequentialAnimation{
        id: anim
        property bool isSeen: false
        property real x
        property real y
        property real distance: 200
        
        function set_and_start(x, y){
            if(anim.x === x && anim.y === y)
                return;
            anim.x = x;
            anim.y = y;
            isSeen = true;
            start();
        }
        
        ParallelAnimation{
            NumberAnimation {
                target: row_helper
                property: "opacity"
                from: 0.0
                to: 1.0
                duration: 500
                easing.type: Easing.InOutQuad
            }
            NumberAnimation {
                target: row_helper
                property: "x"
                from: matrix.x - row_helper.width - anim.distance
                to: matrix.x - row_helper.width
                duration: 500
                easing.type: Easing.InOutQuad
            }
        }
        ScriptAction { script: paint_over(-1, anim.y) }
        ParallelAnimation{
            NumberAnimation {
                target: column_helper
                property: "opacity"
                from: 0.0
                to: 1.0
                duration: 500
                easing.type: Easing.InOutQuad
            }
            NumberAnimation {
                target: column_helper
                property: "y"
                from: matrix.y - column_helper.height - anim.distance
                to: matrix.y - column_helper.height
                duration: 500
                easing.type: Easing.InOutQuad
            }
        }
        ScriptAction { script: paint_over(anim.x, -1) }
        ParallelAnimation{
            NumberAnimation {
                target: rect_helper
                property: "opacity"
                from: 0.0
                to: 1.0
                duration: 500
                easing.type: Easing.InOutQuad
            }
            NumberAnimation {
                target: rect_helper
                property: "x"
                from: matrix.x /*- matrix.cellWidth*/ + ((anim.x - anim.x % 3)/3 + 1) * 3*matrix.cellWidth + anim.distance
                to: matrix.x /*- matrix.cellWidth*/ + ((anim.x - anim.x % 3)/3 + 1) * 3*matrix.cellWidth
                duration: 500
                easing.type: Easing.InOutQuad
            }
        }
        ScriptAction { script: paint_over(anim.x, anim.y) }
    }
    
    function paint_over(x, y)
    {
        if(x !== -1 && y !== -1)
        {
            var rounded_x = x - x%3;
            var rounded_y = y - y%3;
            for(var i = 0; i < 3; i++)
                for(var j = 0; j < 3; j++)
                    matrix.model.get((rounded_y+j)*9+i+rounded_x)["dimed"] = true;
            return;
        }
        
        if(x !== -1)
        {
            for(var i = 0; i < 9; i++)
                matrix.model.get(i*9+x)["dimed"] = true;
            return;
        }
        if(y !== -1)
        {
            for(var j = 0; j < 9; j++)
                matrix.model.get(y*9+j)["dimed"] = true;
            return;
        }
        
        if(x === -1 && y === -1)
        {
            for(var i = 0; i < matrix.model.count; i++)
                matrix.model.get(i)["dimed"] = false;
            return;
        }
    }
    
    Helper{
        opacity: 0
        id:row_helper
        z: matrix.z + 2
        state: "right-oriented"
        //        anchors.right: matrix.left
        sizeOfMatrixCell: matrix.sizeOfCell
        MouseArea{
            anchors.fill: parent
        }
    }
    
    Helper{
        opacity: 0
        id:column_helper
        z: matrix.z + 2
        sizeOfMatrixCell: matrix.sizeOfCell
        state: "bottom-oriented"
    }
    
    Helper{
        opacity: 0
        id: rect_helper
        z: matrix.z + 2
        sizeOfMatrixCell: matrix.sizeOfCell
        state: "left-oriented"
    }
    
    Helper{
        opacity: 0
        id: fallback_helper
        z: matrix.z + 2
        textMode: true
        sizeOfMatrixCell: matrix.sizeOfCell
        state: "top-oriented"
    }
    
    GridView{
        id: matrix
        anchors.centerIn: parent
        property int sizeOfCell: 50
        width: matrix.sizeOfCell * 9
        height: width
        interactive: false
        cellWidth: matrix.sizeOfCell
        cellHeight: cellWidth
        model: ListModel{
            id: myModel
            
            function prepareModel(){
                if(myModel.count == 0)
                    for(var i = 0; i < 81; i++)
                        myModel.append({"number": "", /*"blocked": false, */
                                           "marked": false, "dimed": false,
                                           "isGuess": false})
            }
            
            Component.onCompleted: 
            {
                prepareModel();
            }
        }
        
        Sudoku{
            id: sudoku
            onUpdateVisualState: {
                matrix.setCells(sudoku.getString());
            }
            Component.onCompleted: {
//                sudoku.prepare();
                popup.start_time = new Date().getTime()
            }
        }
        
        function makeItteration()
        {
            sudoku.makeItteration()
            setCells(sudoku.getString())
        }
        
        function clear(){
            for(var i=0; i<81; i++)
            {
                model.set(i, {"number": "", /*"blocked": false, */
                              "marked": false, "dimed": false,
                              "isGuess": false})
            }
        }
        
        function setCells(cellsState){
            myModel.prepareModel();
            for(var i = 0; i < cellsState.length; i++)
            {
                if(cellsState[i] !== '.')
                {
                    model.set(i, {"number": cellsState[i]})
                    //                    model.set(i, {"blocked": true})
                }
                else
                {
                    model.set(i, {"number": ''})
                    //                    model.set(i, {"blocked": false})
                }
            }
        }
        
        function clearEmptyCells(indexesToClear)
        {
            for(var i = 0; i < indexesToClear.length; i++)
            {
                model.set(indexesToClear[i], {"number": ''})
                //                model.set(indexesToClear[i], {"blocked": false})
                model.set(indexesToClear[i], {"isGuess": false})
            }
        }
        
        function insertInModel(x, y, key, value)
        {
            model.get(x+y*9)[key] = value
        }
        
        function setState(){
            var str = '';
            for(var i = 0; i < model.count; i++)
            {
                var element = model.get(i)
                str += element['number'].length > 0 ? element['number'] : '.';
            }
            sudoku.clear();
            var successful = sudoku.setInitialString(str);
            return successful;
        }
        
        delegate: Rectangle{
            
            id: cell
            color: dimed ? "#E0E0E0" : "white"
            width: matrix.sizeOfCell
            height: width
            border.color: "#9E9E9E"
            layer.enabled: true
            property bool isSelected: false
            property bool isGlowing: marked || isSelected
            property bool isGuess: false
            onIsGlowingChanged: {
                if(isGlowing)
                    z+=2;
                else
                    z-=2;
            }
            
            
            Behavior on color { PropertyAnimation {
                    duration: 500
                    easing.type: color !== 'white' ? Easing.OutElastic : Easing.Linear
                }}
            layer.effect: Glow {
                samples: 15
                color: isSelected ? (/*isGuess ? "#9c27b0" : */ "#607D8B") : marked ? "#FFCC80" :  "transparent"
                transparentBorder: true
                Behavior on color { PropertyAnimation {
                        duration: 250
                        easing.type: Easing.Linear
                    }}
            }
            property var isMarked: marked
            //MFD..................................
            //            onIsMarkedChanged: {
            //                if(isMarked)
            //                {
            //                    number = '1'
            //                    cell.userMadeStep()
            //                }
            //            }
            //MFD_END..............................
            
            
            property bool isWrong: false
            property string last_number
            
            Text{   
                id: textElem
                anchors.centerIn: parent
                text: number
                font.pointSize: 26 * matrix.sizeOfCell/50
                horizontalAlignment: Text.AlignHCenter
                //                onTextChanged: {
                //                    if(!text)
                //                        parent.isWrong = false;
                //                }
                
                color: parent.isWrong ? "#B00020" : parent.isGuess ? 
                                            "#9c27b0" : "black"
            }
            function userMadeStep()
            {
                var response = sudoku.userStep(index, number);
                if(response.isFallback)
                {
                    if(response.message === "ok")
                    {
                        hider_fallback_anim.start()
                        matrix.clearEmptyCells(response.cellsIndexesToClear)
                        isWrong = false;
                        marked = false;
                        if(anim.isSeen)
                            hider_anim.start();
                    }
                    else
                    {
                        popup.mistakes_count++;
                        console.log("mistake: fallback required")
                        pointFallbackHelper(response.x, response.y)
                        matrix.insertInModel(response.x, response.y, "marked", true);
                        isWrong = true;
                    }
                    
                }
                else
                {
                    if(response.message === "ok"){
                        isGuess = response.isGuess;
                        isWrong = false;
                        marked = false;
                        if(anim.isSeen)
                            hider_anim.start();
                        if(response.isSolved)
                        {
                            console.log("Solved!")
                            popup.end_time = new Date().getTime()
                            popup.open()
                        }
                    }
                    else{
                        popup.mistakes_count++;
                        matrix.insertInModel(response.x, response.y, "marked", true);
                        isWrong = true;
                        row_helper.setAllowedDigits(response.row)
                        rect_helper.setAllowedDigits(response.rec)
                        column_helper.setAllowedDigits(response.col)
                        mainItem.pointHelpers(response.x, response.y)
                        //MFD..................................
                        //                        if(index === response.x + response.y*9)
                        //                        {
                        //                            number = response.correct_digit.toString();
                        //                            userMadeStep();
                        //                        }
                        //MFD_END..............................
                    }
                }
            }
            
            MouseArea{
                id: m_area
                anchors.fill: parent
                onClicked: {
                    forceActiveFocus()
                    //                    pointFallbackHelper(index- 9*Math.floor(index/9), Math.floor(index/9))
                    //MFD..................................
                    //                    number = '1'
                    //                    cell.userMadeStep()
                    //MFD_END..............................
                }
                
                onFocusChanged:
                {
                    if(focus)
                        parent.isSelected = true
                    else
                    {
                        if(parent.isWrong)
                        {
                            parent.isWrong = false;
                            number = cell.last_number;
                        }
                        
                        parent.isSelected = false
                    }
                }
                
                Keys.onPressed: {
                    //                    if(!blocked){
                    if(!cell.isWrong)
                        cell.last_number = number;
                    if(event.text > 0 && event.text <= 9)
                        number = event.text;
                    else if(event.key === Qt.Key_Backspace || 
                            event.key === Qt.Key_Delete ||
                            event.key === Qt.Key_Space ||
                            event.key === Qt.Key_0)
                        number = ''
                    if(!custom_initial)
                        cell.userMadeStep();
                    //                    }
                }
            }
        }
        
        property int widthOfBorder: matrix.sizeOfCell/18
        Rectangle{
            x: 0
            y: matrix.height/3
            height: matrix.widthOfBorder
            width: matrix.width
            color: "black"
        }
        Rectangle{
            x: 0
            y: matrix.height*2/3
            width: matrix.width
            height: matrix.widthOfBorder
            color: "black"
        }
        Rectangle{
            x: matrix.width/3
            y: 0
            color: "black"
            height: matrix.height
            width: matrix.widthOfBorder
        }
        Rectangle{
            x: matrix.width*2/3
            y: 0
            color: "black"
            height: matrix.height
            width: matrix.widthOfBorder
        }
    }
    

        Button{
            visible: custom_initial
            text: "Подтвердить"
anchors.top: matrix.bottom
        anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                var successful = matrix.setState();
                if(successful)
                {
                    successful = sudoku.solveAndShow();
                }
                if(!successful)
                    popup_error.open()
                else{
                    custom_initial = false;
                }
            }
        }
}
