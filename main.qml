import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 2.4

Window {
    visible: true
    width: 960
    height: 540
    title: qsTr("Sudoku")
    Item {
        id: name
        anchors.fill: parent
        MouseArea{
            anchors.fill: parent
            onClicked: {focus = true; console.log("should be defocused")}
        }
        
        SwipeView{
            id: swipe
            anchors.fill: parent
            Item {
                Rectangle{
                    anchors.centerIn: parent
                    width: row.width+row.spacing*2
                    height: row.height+row.spacing*2
                    border.color: "#9E9E9E"
                    BusyIndicator{
                        id: busy
                        running: true
                        visible: false
                        anchors.bottom: row.top
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                Row{
                        id: row
                    spacing: gen_button.height/2
                    anchors.centerIn: parent
                    Button{
                        id: gen_button
                        text: "Сгенерировать судоку"
                        onClicked: {
                            busy.visible = true
                            matrix.clearAll()
                            matrix.prepareSudoku()
                            busy.visible = false
                            swipe.incrementCurrentIndex()
                        }
                    }

                    Button{
                        text: "Ввести судоку"
                        onClicked: {
                            matrix.custom_initial = true
                            swipe.incrementCurrentIndex()
                        }
                    }
                }
                }
            }

            Item{
            Matrix{
                id: matrix
                anchors.centerIn: parent
            }
            }
        }
        
        //        Row{
        //            anchors.horizontalCenter: parent.horizontalCenter
        //            anchors.top: matrix.bottom
        //            Button{
        //                text: "Set initial"
        //                onClicked: matrix.setState();
        //            }
        //            Button{
        //                text: "Make itteration"
        //                onClicked: matrix.makeItteration();
        //            }
        //            Button{
        //                text: "Clear"
        //                onClicked: matrix.clear();
        //            }
        //        }
                Text{
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    textFormat: Text.RichText;
                    text: "author: <style>a:link { color: #5f6368 ; }</style>" +
                          "<a href=" + link + ">Kiops</a>";
        //            text: "author: Kiops"
                    color: "#5f6368"
                    property url link: "https://vk.com/kiops"
                    onLinkActivated: Qt.openUrlExternally(link)
                    anchors.margins: height*0.5
                }
    }
}
